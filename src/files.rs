use std::path::{Path, PathBuf};
use std::{io, vec::Vec, fs, env};

use regex::Regex;
use rocket::data::{Data, ToByteUnit};
use rocket::serde::Serialize;
use rocket::serde::json::Json;

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
struct DirEntry {
    name: PathBuf,
    is_dir: bool,
}

fn handle_dir_entries(entries: fs::ReadDir) -> Vec<DirEntry> {
    let mut entry_list = Vec::new();
    for entry in entries {
        match entry {
            Ok(dir_entry) => {
                let path = dir_entry.path();
                entry_list.push(DirEntry {
                    name: path.to_path_buf(),
                    is_dir: path.is_dir(),
                });
            },
            Err(_) => {},
        }
    }

    entry_list
}

#[get("/<path..>")]
fn get_in_dir(path: PathBuf) -> io::Result<Json<Vec<DirEntry>>> {
    println!("reading path: {:?}", path);
    let entries = fs::read_dir(path)?;
    Ok(Json(handle_dir_entries(entries)))
}

#[post("/<path..>", data = "<file>")]
async fn upload(path: PathBuf, file: Data<'_>) -> io::Result<()> {
    if path.is_dir() {
        return Err(
            io::Error::new(
                io::ErrorKind::Other,
                "Given path leads to an existing directory"
            )
        );
    };

    let filename_regex = Regex::new(r"/?([a-z0-9.\-_ ]+)$").unwrap();

    let filepath = path.to_str().unwrap();

    let filename_match = filename_regex.find(filepath).unwrap();

    filename_match.start();

    let leading_slash_regex = Regex::new(r"^/+").unwrap();

    let filename = &leading_slash_regex.replace(&filepath[filename_match.start() .. filename_match.end()], "");
    let stripped_path = &filepath[.. filename_match.start()];

    let base_dir = match env::var("FILE_DIR") {
        Ok(dir) => dir,
        Err(_) => String::from("./"),
    };

    let trimmed_path = leading_slash_regex.replace(stripped_path, "");
    let target_dir = base_dir + &trimmed_path;

    if !Path::new(&target_dir).is_dir() {
        return Err(
            io::Error::new(
                io::ErrorKind::Other,
                "Given target directory does not exist",
            )
        );
    };

    file.open(10.gigabytes()).into_file(target_dir + "/" + filename).await?;
    Ok(())
}

pub fn stage() -> rocket::fairing::AdHoc {
    rocket::fairing::AdHoc::on_ignite("files", |rocket| async {
        rocket.mount("/files", routes![get_in_dir, upload])
    })
}