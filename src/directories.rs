use std::{io, env, fs};

use regex::Regex;

use rocket::serde::json::Json;
use rocket::serde::{Deserialize, Serialize};

#[derive(Serialize)]
#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
struct Directory {
    path: String,
}

#[post("/", data="<dir>")]
fn new(dir: Json<Directory>) -> io::Result<Json<Directory>> {
    let base_dir = match env::var("FILE_DIR") {
        Ok(dir) => dir,
        Err(_) => String::from("./"),
    };
    let leading_slash_regex = Regex::new(r"^/+").unwrap();
    let trimmed_path = leading_slash_regex.replace(&dir.path, "");
    let new_dir_path = base_dir + &trimmed_path;
    println!("Creating new Folder \"{}\"", new_dir_path);
    fs::create_dir_all(&new_dir_path)?;
    Ok(Json(Directory { path: new_dir_path }))
}

pub fn stage() -> rocket::fairing::AdHoc {
  rocket::fairing::AdHoc::on_ignite("directories", |rocket| async {
    rocket.mount("/directories", routes![new])
  })
}