#[macro_use] extern crate rocket;

mod directories;
mod files;

#[launch]
fn rocket() -> _ {
    rocket::build()
        .attach(directories::stage())
        .attach(files::stage())
}